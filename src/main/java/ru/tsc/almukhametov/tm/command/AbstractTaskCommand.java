package ru.tsc.almukhametov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void showTasks(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[FIND PROJECT]");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
