package ru.tsc.almukhametov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import static ru.tsc.almukhametov.tm.constant.TerminalConst.BACKUP_LOAD_XML;
import static ru.tsc.almukhametov.tm.constant.TerminalConst.BACKUP_SAVE_XML;

public class Backup extends Thread {

    @NotNull
    public final Bootstrap bootstrap;
    private final int interval = 30000;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            save();
            Thread.sleep(interval);
        }
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommands(BACKUP_LOAD_XML);
    }

    public void save() {
        bootstrap.parseCommands(BACKUP_SAVE_XML);
    }
}
