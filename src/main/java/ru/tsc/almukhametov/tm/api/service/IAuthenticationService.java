package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IAuthenticationService {

    @NotNull
    Optional<User> getCurrentUserId();

    void setCurrentUserId(@NotNull String userId);

    void checkRoles(@Nullable Role... roles);

    boolean isAuthentication();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@NotNull String login, @NotNull String password, @NotNull String email);

}
